#
# Experiments running framework, not (yet) platform-independent, works on unix systems
#
# Author: Venkat
# 

#####################
# Experiment module #
#####################

import sys
import os
import shlex
import subprocess
import threading
import time

from utils import *

#############################

g_sshDomainNames = dict() # shortName --> DNSDomainNames

#FIXME parameterize this ssh cmd somewhere appropriate
g_sshCmd ="ssh "
#g_sshCmd ="ssh -i $ssh-key "

g_metaLogFile = "experiment.log"

#g_trace_bin = "<path-to>/lttng-trace.sh"
#g_trace_bin = "<path-to>/ftrace.sh"
g_trace_bin = "<path-to->/xen-trace.sh"

g_host_id = 'hostdom'

def log(logStr, oFile=g_metaLogFile):        
    appendToFile(oFile, logStr)


class ETask():
    """
    Class to describe a primitive Experiment Task (ETask) which runs asynchronously
    on the specified machine, with the executable
    """
    bin = '/bin/true/' # binary to execute for this task
    args = ' ' # arguments for the above binary

    wd = '' # experiment working directory path

    where = 'local' # None - for local run, else domain-name of the machine
    #asWho = '' # ssh as which user - username
   
    times = 3 # execute how many times?

    name = None # log output to this file, don't if None -- try to log as atomically (quickly, append-close)?
    isTimed = True # time the execution using python timer
    isBG = False # Background process
    collectTrace = False # collect lttng or whatever tracing tool
    startDelay = 0 # used in cases where time delay is important for the task to initialize and start running

    tasklet = None
    timedTask = None
    bgTask = None

    startTime = 0
    runTimes = []

    def __str__(self):
        pp_str = '[ ' + self.bin + ' ' + self.args + ' ] @ ' + self.where + ' : ' + self.name;
        tmp = ' BG\n' if self.isBG else ' FG\n'
        return pp_str + tmp

    def __repr__(self):
        return self.__str__()

    def __init__(self, bin, args, where='local', isTimed=True, isBG=False, times=3, startDelay=0, name='unamed', collectTrace = False):
        self.bin = bin
        self.args = args
        self.wd = os.getcwd()

        self.where = where
        #self.asWho = asWho
        self.times = -1 if isBG else times
        self.name = name
        self.isTimed = isTimed
        self.isBG = isBG
        self.startDelay = startDelay
        self.collectTrace = collectTrace
        
    def run(self, exp):

        #Log the task spec before running... meta-log, not the name
        log(self.bin + " " + self.args + " @ " + self.where + \
                     " : " + self.name)
        log(" BG\n" if self.isBG else " FG\n")

        print "Started ETask: " + self.bin + " @ " + self.where

        if self.where == 'local':
            #Local Exec
            self.tasklet = Exec( self.bin, self.args )
        elif g_sshDomainNames.has_key(self.where) == False:
            print "FATAL ERROR: DNS Domain Name for " + self. where + " is missing.."
            return -1
        else:
            # Remote Exec
            self.tasklet = SshExec( g_sshCmd + g_sshDomainNames[self.where][0], \
                                    self.bin, self.args , g_sshDomainNames[self.where][1])

        if self.isTimed and not self.isBG: ##assert (not isBG), "Timed Task cannot be a Background task"
            assert (self.times > 0), "BAD SPEC: Should be at least run one time"

            def runNTimes():
                for i in xrange(self.times):
                    #print (i+1)

                    self.tasklet.run()
                    self.runTimes.append(self.tasklet.runTime)

                    #Log Output for timed FG tasks here
                    #FIXME: make this log name configurable
                    log(self.name + " : " + str(self.runTimes) + "\n", self.wd + "/" + exp.current_log_name)
                    log(self.tasklet.stdout + "-------\n" + self.tasklet.stderr, self.wd + "/" +  exp.current_log_name)
                    
            self.timedTask = threading.Thread(target=runNTimes, name="runNTimes")
            self.timedTask.start()
        else:
            #BG or non timed FG tasks
            assert (self.times <= 0), "BAD SPEC: Times should be 0 for BG or non-timed FG tasks"
            #self.tasklet.start()
            def runInBG(expTask):
                expTask.tasklet.run()
                time.sleep(expTask.startDelay)
            
                log(expTask.name + " : " + str(expTask.runTimes) + "\n", expTask.wd + "/" + expTask.name)
                log(expTask.tasklet.stdout + "-------\n" + expTask.tasklet.stderr, expTask.wd + "/" + expTask.name)
                return;

            self.bgTask = threading.Thread(target=runInBG, name="runInBG", args=(self,))
            self.bgTask.start()
            
        return 0

    def done(self, exp):
        """
        Kills Daemons (background processes) but waits for foreground processes
        """
        if self.tasklet == None:
            print "ERROR: Task was not started!"
            return -1

        if(self.timedTask == None):
            if(self.bgTask.isAlive() \
                   and self.isBG):
                print "Killing background process... " + self.bin + " @ " + self.where
                self.tasklet.kill()
                self.bgTask.join(5)
                
        
            #FIXME: Log Output for BG or nonTimed FG tasks here
            #log(exp.current_log_name + " : " + self.tasklet.stdout + "\n------------\n" + self.tasklet.stderr + "\n" , )

        elif(self.timedTask.isAlive()):            
            if self.isTimed or not self.isBG:
                print "Waiting for timed task to complete... " + self.bin + " @ " + self.where
                self.timedTask.join()
                
            

        print " ...done";
        return 0

                               
    
class Experiment():
    resultRoot = '.'
    resultDir = ''
    user = ''
    eTaskList = []
    trace_num = 0
    current_log_name = 'tmp'

    def metaLog(self, logStr):        
        appendToFile(self.resultRoot + '/' + g_metaLogFile, logStr)


    def __str__(self):
        task_list_str = ''

        for currTask in self.eTaskList:
            (name, tasklist) = currTask
            task_list_str = task_list_str + '\tname: ' + str([str(item) for item in tasklist]) + ',\n'

        pp_str = "<%s>:\n" % self.resultDir + "{ " + task_list_str+ " }\n"
        return pp_str

    def __repr__(self):
        return self.__str__()
        
    def __init__(self, resultRoot='.', expName='expname', user='ubuntu', dNameFile='iDomainMap.in'):
        self.resultDir =  expName + '-' + timestamp()
        self.resultRoot = resultRoot
        self.user = user
        self.trace_num = 0

        self.loadDomainNames(dNameFile)

        if os.path.isdir(self.resultRoot + '/' + self.resultDir):
            print self.resultRoot + '/' + self.resultDir + " directory already exists! exitting"
            exit

        print '=== Logged at ' + self.resultRoot + '/' + self.resultDir + ' ===\n'

        os.mkdir(self.resultRoot + '/' + self.resultDir)
        os.chdir(self.resultRoot + '/' + self.resultDir)

        #FIXME: log the experiment spec before running... meta-log
        
        #FIXME: check if the git repo is updated

        #Log Timestamp, git repo id, 
        self.metaLog(self.resultRoot + '/' + self.resultDir + " -- ")
        log(self.resultDir + ":\n\nDomain Mapping:\n" + str(g_sshDomainNames) + "\n\n", \
            self.resultRoot + '/' + self.resultDir + "/log.txt")
        log(str(self), \
            self.resultRoot + '/' + self.resultDir + "/log.txt")
 
    def setupTaskList(self):   
        pass

    def preLoop(self):
        pass

    def postLoop(self):
        pass

    def startTrace(self):
        ##lttng create experiments; lttng enable-event sched_switch,sched_stat_runtime,sched_migrate_task,sched_wakeup -k;
        print "Starting trace"
        trace_util = Exec( g_trace_bin, " start " + g_sshDomainNames[g_host_id][0] +\
                          " " + self.resultDir + "_" + str(self.trace_num) ) 
                           #+ " sched_switch sched_stat_runtime sched_migrate_task sched_wakeup")
        trace_util.run()
        self.trace_num = self.trace_num + 1

    def stopTrace(self):
        print "Stopping trace ..."
        trace_util = Exec(g_trace_bin, " stop " + g_sshDomainNames[g_host_id][0] +\
                         " " + self.resultDir + "_" + str(self.trace_num-1) +\
                         " " + self.resultRoot + "/" + self.resultDir)
        trace_util.run()
        log("Tracefile: " + self.resultDir + "_" + str(self.trace_num) + "\n", self.resultRoot + '/' + self.resultDir + "/log.txt")
        log("\Trace Util Output: \n" + trace_util.stdout + "-------\n" + trace_util.stderr)

    def mainLoop(self):
        """
        Iterates through all the tasksets and executes them one by one
         - Waits for foregrounds to get over before killing the background processes in each taskset
         - Task launch sequence respects the order in the taskset
        """
        
        prev_str = ""
        for (name,currTaskSet) in self.eTaskList:
            trace = False
            for etask in currTaskSet:
                trace = trace or etask.collectTrace
            if trace:
                exp_summary = "%s %s (w/trace)," % (prev_str, name)
            else:
                exp_summary = "%s %s," % (prev_str, name)

            prev_str = exp_summary
        exp_summary = "%s\n" % prev_str 
        self.metaLog(exp_summary)

        for (name,currTaskSet) in self.eTaskList:
            trace = False
    
            self.current_log_name = name

            print "Running ==> %s <== experiment..\n" % name
            for etask in currTaskSet:
                trace = trace or etask.collectTrace

            if trace:
                self.startTrace()

            for etask in currTaskSet:
                etask.run(self)
                
            ##FIXME: The below two loops can be combined into one if the taskset is ordered correctly
            ##       (or if that assumption is checked)

            for etask in currTaskSet:
                if(not etask.isBG):
                    etask.done(self)

            for etask in currTaskSet:
                if(etask.isBG):
                    etask.done(self)                


            if trace:
                self.stopTrace()
        
            #print "DONE!\nActive threads:\n"
            #for thr in threading.enumerate():
            #    print thr.name


    def run(self):
        
        self.setupTaskList()

        self.preLoop()

        self.mainLoop()
        
        self.postLoop()

        
    def loadDomainNames(self, iFile):

        if(not os.access(iFile, os.F_OK)):
            print iFile + " inaccessible, exitting.."
            exit

        inputFile = open(iFile, 'r')
        g_sshDomainNames.clear()

        for names in inputFile:
            #print names
            namemap = names.strip().split('\t')
            if len(namemap) < 3:
                continue;
            if namemap[0][0] == '#':
                continue;
            g_sshDomainNames[namemap[0]] = ( namemap[1] + "@" + namemap[2], namemap[1] )

        # print "Done loading the Domain Name maps ... \n"
        # print ' '.join(g_sshDomainNames.keys())
        # print ' '.join(g_sshDomainNames.values())

    
    def killall(self):
        for key in g_sshDomainNames.keys():
            if key != g_host_id or 'root' in key:
                #print "killing proc belonging to %s @ %s" % (g_sshDomainNames[key][0], g_sshDomainNames[key][1])
                sshkill(g_sshCmd + g_sshDomainNames[key][0], g_sshDomainNames[key][1])
            

