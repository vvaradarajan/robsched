#!/bin/sh

# ARGS:
#
# <trace-cmd> <host-to-run-tracing> <log-file-name> <dir-to-copy-the-log-file>
#

#
# NOTE:
# Requires xen-formats file to format the binary log file into plaintext logs

#FIXCONFIG
result_dir="<xentrace-results-dir>"

if [ "$1" = "start" ]
then
    
    host=$2
    log_file=$3
    ssh $host  -t -t <<EOF

xentrace -D -e 0x0002f000 -s 1000 $result_dir/${log_file}.out &> /dev/null < /dev/null &
pidof xentrace > xentrace.pid 
cat xentrace.pid
exit;
EOF
exit;
fi

if [ "$1" = "stop" ]
    then
    host=$2
    log_file=$3
    ssh $host -t -t <<EOF
#kill -s INT `cat xentrace.pid `
killall -s INT xentrace
xentrace_format ~/formats > $result_dir/${log_file}.log < ${result_dir}/${log_file}.out
exit;
EOF

scp $host:$result_dir/$log_file.* $4/;
exit
fi


echo "ERROR: Incorrect usage...";
