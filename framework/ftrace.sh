#!/bin/sh

# ARGS:
#
# <trace-cmd> <host-to-run-tracing> <log-file-name> <dir-to-copy-the-log-file>
#

#FIXCONFIG
experiment_traces_dir = "<path-to-experiment-traces-dir>"

if [ "$1" = "start" ]
then
    
    host=$2
    log_file=$3
    shift;shift;shift;
    events=$*
    echo "$events"
    ftrace_opts=`echo "$events" | sed 's/ / -e /g';`
    ssh $host -t <<EOF

#nohup trace-cmd record -e $ftrace_opts  > trace-cmd.out 2> trace-cmd.err < /dev/null &
trace-cmd start -e $ftrace_opts;
exit
EOF
exit;
fi

if [ "$1" = "stop" ]
    then
    host=$2
    log_file=$3
    ssh $host -t <<EOF
#kill -INT `pidof trace-cmd`
trace-cmd stop
trace-cmd extract -o $experiment_traces_dir/${log_file}.dat > trace-cmd.out 2> trace-cmd.err;
trace-cmd report -i $experiment_traces_dir/${log_file}.dat  > $experiment_traces_dir/${log_file}.out;
./get_vm_tasks.sh > $experiment_traces_dir/${log_file}.cfg
exit;
EOF
scp $host:$experiment_traces_dir/$log_file.* $4/;
exit
fi


echo "ERROR: Incorrect usage...";
