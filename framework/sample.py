
#
# Sample Experiment class instantiation
#


from experiment import *

workloadRoot = 'workloads/'
  
#benchmarks
#FIXCONFIG
benchmark_root = '<benchmark-root-dir>'

#specbench
specbench_root = workloadRoot + '/specbench/'

specJBB_bin = specbench_root + 'specJBB/run.sh '
specJBB_arg = ' '


cpu_nqueens_bench = workloadRoot + 'nqueens'
cpu_nqueens_burn_arg = ' 14 -1 '

#FIXCONFIG
mem_cprobe_burn_bin = workloadRoot + 'cprobe'
mem_cprobe_burn_arg = ' %d ' % (<cache-size-kb>* 1024 )


class SampleExperiment(Experiment):
    
    def __init__(self, resultRoot, expName='schedexp'):
        Experiment.__init__(self, resultRoot, expName, user='venkatv', dNameFile='iDomainMap-sc-h07.in')

        
    def setupTaskList(self):    

        self.eTaskList.append(('specJBB-baseline', \
               [
                ETask(specJBB_bin, specJBB_arg, 'vm01', True, False, 1, name='specJBB'),
               ]))

#         self.eTaskList.append(('specJBB-vs-cprobe', \
#                [
#                 ETask(specJBB_bin, specJBB_arg, 'vm01', True, False, 1, name='specJBB'),
#                 ETask(mem_cprobe_burn_bin, mem_cprobe_burn_arg, 'vm02', False, True, 1, name='vm2-cache'),
#                ]))

        self.eTaskList[0][1].extend(
            [
             ETask('xl', 'sched-credit' , 'hostdom', True, False, 1, name='xen-params'),
             ETask('xl', 'list' , 'hostdom', True, False, 1, name='xen-params'),
             ETask('xl', 'vcpu-list' , 'hostdom', True, False, 1, name='xen-params'),
            ])


    def preLoop(self):
        self.killall()
        self.killall()


###############################################################################
# Main
###############################################################################
if __name__ == '__main__' : 

    #FIXCONFIG
    SampleExperiment('<path-to-experiment-log-dir>', dNameFile='domainNames.in').run()
