#
# Utilities for the experiments running framework
#
# Author: Venkat
# 


import sys
import os
import shlex
import subprocess
import threading
import time
import datetime
import signal


####################
# Utilities module #
####################


####################
# Helper Functions #
####################

def timestamp():
    date = datetime.datetime.now()
    return date.strftime("%Y%m%d-%2H%2M%2S")

def appendToFile(name, text):  
    file = open(name , 'a')  
    file.write(text)
    file.close()
  
def sshkill(ssh, user, tokill=''):
    #print ssh + ' "killall -u %s -q %s"' % (user, tokill)
    ssh += ' -o ConnectTimeout=1 '
    if user == 'root':
        if tokill != '':
            p = subprocess.Popen(shlex.split(ssh + ' "killall %s"' % (tokill)))
            p.wait()
        #else: do nothing if it is root user
        return
    p = subprocess.Popen(shlex.split(ssh + ' "killall -u %s -q %s"' % (user, tokill)))
        
    p.wait()



####################
# Helper Classes #
####################


class Exec(threading.Thread):
  def __init__(self, cmd, args):
    threading.Thread.__init__(self)    
    self.cmd = cmd
    self.args = args
    self.work = shlex.split("%s %s" % (cmd, args))
    self.stdout = None
    self.stderr = None
    self.child = None

    self.runTime = 0

  def run(self):    
      #print self.work
      #print "--------"

      startTime = time.time()
      self.child = subprocess.Popen(self.work, #shell=True,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE, 
                                    preexec_fn=os.setsid)
      
      (self.stdout, self.stderr) = self.child.communicate()
      self.runTime = time.time() - startTime

  # def run_in_bg(self):
  #     self.child = subprocess.Popen(self.work, shell=True,
  #                                 stdout=subprocess.PIPE,
  #                                 stderr=subprocess.PIPE)#, preexec_fn=os.setsid)

  def abort(self):
    """ may be unsafe to call"""
    if self.child:
      self.child.terminate()
    
  # def send_SIGINT(self):
  #   if self.child:
  #     self.child.send_signal(signal.SIGINT)

      #for line in self.child.stdout.readlines():
      #    self.stdout = self.stdout + line


  def kill(self):
    """ Kill all target process"""

    p =  subprocess.Popen(shlex.split('killall -INT -q  %s' % (self.cmd)))
    p.wait()      

    self.child.wait() # make sure the killed process joins..? I don't know if this makes a difference!
    

class SshExec(Exec):
  def __init__(self, ssh, cmd, args, user):
    Exec.__init__(self, cmd, args)
    self.ssh = ssh    
    self.work = shlex.split(ssh + ' "%s %s"' % (cmd, args))
    self.stdout = ''
    self.stderr = ''  
    self.user = user
    
  def kill(self):
    tries = 0
    sig="INT"
    while(True):

        p = subprocess.Popen(shlex.split(self.ssh + ' "killall -%s -q %s"' % (sig,self.cmd)))
        #print "Killing... %s" % self.ssh + ' "killall -INT -q -g %s"' % (self.cmd)

        p.wait()
        #print "Return code: %d" % p.returncode

        #p = subprocess.Popen(shlex.split(self.ssh + ' "/sbin/pidof %s"' % (self.cmd)))
        #stdout, stderr = p.communicate()
        #p.wait()
        #print "%s \n----------------\n%s\n" % (stdout,stderr)

            
        if(p.returncode != 0 or tries > 4):
            break;
        tries += 1
        if(tries == 3):
            sig="TERM"
        

    if tries > 4:
        self.child.terminate()
    #(self.stdout, self.stderr) = self.child.communicate()

    #p.wait()  
    #self.child.wait()
    #self.join()  # make sure the killed process joins..?
    
