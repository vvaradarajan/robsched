#!/bin/sh

# ARGS:
#
# <trace-cmd> <host-to-run-tracing> <log-file-name> <dir-to-copy-the-log-file>
#

#FIXCONFIG
experiment_traces_dir = "<path-to-experiment-traces-dir>"

if [ "$1" = "start" ]
then
    
    host=$2
    log_file=$3
    ssh $host -t <<EOF
lttng destroy experiments;
#rm -rf /root/lttng-traces/experiments*;
lttng create experiments;
lttng set-session experiments;
lttng enable-event $4 -k;
lttng list experiments  >> $experiment_traces_dir/$log_file.log;
lttng start;
EOF
exit;
fi

if [ "$1" = "stop" ]
    then
    host=$2
    log_file=$3
    ssh $host -t <<EOF
lttng stop;
babeltrace $experiment_traces_dir/experiments-* > $experiment_traces_dir/${log_file}.out;
./get_vm_tasks.sh > $experiment_traces_dir/${log_file}.cfg
exit;
EOF
scp $host:$experiment_traces_dir/$log_file.* $4/;
exit
fi


echo "ERROR: Incorrect usage...";
